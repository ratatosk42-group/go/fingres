//    Copyright © 2016  Guillaume Zugmeyer
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

// Build information. Populated at build-time.
var (
	BinName   string
	Version   string
	Revision  string
	Branch    string
	BuildUser string
	BuildDate string
	GoVersion = runtime.Version()
)

var (
	rootpath          = flag.String("path", ".", "The rootpath folder path to check")
	filter            = flag.String("filter", "", "Filter files by extension (comma separated)")
	exclude           = flag.String("exclude", "", "Exclude some path (comma separated)")
	nofollow          = flag.Bool("nofollow", false, "Don’t follow symlinks")
	onlymatchinglines = flag.Bool("only-matching-lines", false, "Don’t display line nmuber or filename")
	onlyfilenames     = flag.Bool("only-matching-filename", false, "Display only matching filename")
	respectcase       = flag.Bool("case", false, "Respect case")
	nostandardexclude = flag.Bool("no-exclude-standard", false, "Don’t exclude standard pattern (.tags,.git/,.svn/,.hg/,CVS/,.DS_Store/,vendor/)")
	changeexclude     = flag.Bool("exclude-change", false, "Standard exclude pattern for RBS Change (cache/,build/,repository/,log/,pear/)")
	replace           = flag.String("replace", "", "Replace matching pattern")
	infile            = flag.Bool("infile", false, "Replace in file")
	zshcompletion     = flag.Bool("zsh-completion", false, "Generate zsh completion")

	printVersion = flag.Bool("version", false, "Print current version and build info")
	logFile      = flag.String("log", "/dev/stdout", "Path to log file")
	debug        = flag.Bool("debug", false, "Debug mode")
	selfUpdate   = flag.Bool("self-update", false, "Get new version")

	gitLabURI   = flag.String("gitlab-uri", "https://git.zug.fr", "GitLab base URI")
	gitLabToken = flag.String("gitlab-token", "", "GitLab private token")

	regex     string
	filters   []string
	excluded  []string
	err       error
	re        *regexp.Regexp
	foundFile = make(map[string]bool)
	found     uint
)

func init() {
	flag.StringVar(rootpath, "p", ".", "The rootpath folder path to check")
	flag.StringVar(filter, "f", "", "Filter files by extension (comma separated)")
	flag.StringVar(exclude, "x", "", "Exclude some path (comma separated)")
	flag.BoolVar(nofollow, "nf", false, "Don’t follow symlinks")
	flag.BoolVar(onlymatchinglines, "ol", false, "Don’t display line nmuber or filename")
	flag.BoolVar(onlyfilenames, "of", false, "Display only matching filename")
	flag.BoolVar(respectcase, "c", false, "Respect case")
	flag.BoolVar(nostandardexclude, "nxs", false, "Don’t exclude standard pattern (.tags,.git/,.svn/,.hg/,CVS/,.DS_Store/,vendor/)")
	flag.BoolVar(changeexclude, "xc", false, "Standard exclude pattern for RBS Change (cache/,build/,repository/,log/,pear/)")
	flag.StringVar(replace, "r", "", "Replace matching pattern")
	flag.BoolVar(infile, "i", false, "Replace in file")

	flag.VisitAll(func(f *flag.Flag) {
		envname := fmt.Sprintf("I3_%s", strings.ToUpper(strings.Replace(f.Name, "-", "", -1)))
		envvalue, filled := os.LookupEnv(envname)
		if filled {
			err := f.Value.Set(envvalue)
			if err != nil {
				fmt.Println(fmt.Errorf("invalid value \"%s\" for environment vars %s: %s", envvalue, envname, err))
				flag.Usage()
				os.Exit(2)
			}
		}
	})
}

func main() {

	flag.Parse()

	if *printVersion {
		fmt.Println(BinName, "version", Version)
		fmt.Println("Build by", BuildUser, "the", BuildDate, "from", Branch, "branch with", GoVersion)
		fmt.Println("Revision:", Revision)
		os.Exit(0)
	}

	if *zshcompletion {
		generateZshCompletion(os.Args[0])
		os.Exit(0)
	}

	if *selfUpdate {
		runSelfUpdate()
	}

	logFileHandler, err := os.OpenFile(*logFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	defer logFileHandler.Close()
	log.SetOutput(logFileHandler)
	if *debug {
		log.SetFlags(log.LstdFlags | log.Lshortfile)
	}

	switch i := len(flag.Args()); i {
	case 0:
		fmt.Println("search pattern can’t be null")
		os.Exit(1)
	default:
		regex = flag.Args()[0]
	}

	if !*respectcase {
		regex = "(?i)" + regex
	}
	re, err = regexp.Compile(regex)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if *filter != "" {
		filters = strings.Split(*filter, ",")
	}
	if *exclude != "" {
		excluded = strings.Split(*exclude, ",")
	}

	if !*nostandardexclude {
		excluded = append(excluded, ".tags")
		excluded = append(excluded, ".git/")
		excluded = append(excluded, ".svn/")
		excluded = append(excluded, ".hg/")
		excluded = append(excluded, "CVS/")
		excluded = append(excluded, ".DS_Store/")
		excluded = append(excluded, "vendor/")
	}

	if *changeexclude {
		excluded = append(excluded, "cache/")
		excluded = append(excluded, "build/")
		excluded = append(excluded, "repository/")
		excluded = append(excluded, "log/")
		excluded = append(excluded, "pear/")
	}

	if *infile && *replace == "" {
		fmt.Println("flag infile require replace flag.")
		os.Exit(1)
	}

	err = filepath.Walk(*rootpath, grepFile)
	if err != nil {
		fmt.Println(err)
	}

	if !*onlymatchinglines && !*onlyfilenames {
		if found == 0 {
			fmt.Println("No match")
		} else {
			fmt.Println(found, "matches in", len(foundFile), "files")
		}
	}
}

func matchFilters(path string, filters []string) bool {
	if len(filters) == 0 {
		return true
	}
	for _, filter := range filters {
		if filepath.Ext(path) == "."+filter {
			return true
		}
	}
	return false
}

func notExcluded(path string, excluded []string) bool {
	if len(excluded) == 0 {
		return true
	}
	for _, fragment := range excluded {
		if strings.HasPrefix(path, fragment) || strings.Contains(path, "/"+fragment) {
			return false
		}
	}
	return true
}

func grepFile(path string, info os.FileInfo, err error) error {
	if info == nil {
		return nil
	}
	if info.Mode().IsRegular() && matchFilters(path, filters) && notExcluded(path, excluded) {
		file, _ := os.Open(path)
		defer file.Close()

		var fileCopy *os.File

		if *infile {
			fileCopy, _ = os.Create(path + ".fgrep-temp")
			defer fileCopy.Close()
		}

		buffer := make([]byte, 512)
		n, err := file.Read(buffer)
		if err != nil && err != io.EOF {
			return nil
		}
		contentType := http.DetectContentType(buffer[:n])
		if !strings.HasPrefix(contentType, "text/plain") && !strings.HasPrefix(contentType, "text/xml") && !strings.HasPrefix(contentType, "text/html") {
			return nil
		}
		file.Seek(0, 0)

		reader := bufio.NewReader(file)

		var writer *bufio.Writer
		if *infile {
			writer = bufio.NewWriter(fileCopy)
		}

		numLine := 1
		for {
			line, err := reader.ReadString(byte('\n'))
			if err == nil {
				err = handleLine(path, numLine, line, writer)
				if err != nil {
					return err
				}
			} else {
				if err == io.EOF {
					err = handleLine(path, numLine, line, writer)
					if err != nil {
						return err
					}
					break
				}
				return nil
			}
			numLine++
		}
		if *infile {
			writer.Flush()
			os.Rename(path+".fgrep-temp", path)
		}
		if _, ok := foundFile[path]; ok && !*onlymatchinglines && !*onlyfilenames {
			fmt.Println()
			return nil
		}
	}
	if !*nofollow && info.Mode()&os.ModeSymlink != 0 {
		err := filepath.Walk(path+"/", grepFile)
		if err != nil {
			fmt.Println(err)
		}
	}
	return nil
}

func handleLine(path string, numLine int, line string, writer *bufio.Writer) error {
	if re.MatchString(line) {
		printResult(path, numLine, line)
	}
	if *infile {
		cleanLine := ""
		if *replace != "" {
			cleanLine = re.ReplaceAllString(line, *replace)
		} else {
			cleanLine = line
		}
		_, err := writer.WriteString(cleanLine)
		if err != nil {
			return err
		}
	}
	return nil
}

func printResult(path string, numLine int, line string) {
	if _, ok := foundFile[path]; !ok {
		shortedPath := path
		if *rootpath != "." {
			shortedPath = strings.Replace(path, *rootpath, "", 1)
		}
		if !*onlymatchinglines && !*onlyfilenames {
			fmt.Println("\x1b[34m" + shortedPath + "\x1b[0m")
		}
		if *onlyfilenames {
			fmt.Println(shortedPath)
		}
		foundFile[path] = true
	}
	found++
	if !*onlyfilenames {
		if *onlymatchinglines {
			if *replace != "" {
				cleanLine := re.ReplaceAllString(line, *replace)
				fmt.Print(cleanLine)
			} else {
				fmt.Print(line)
			}
		} else {
			removeSpace := regexp.MustCompile("^\\s+")
			cleanLine := removeSpace.ReplaceAllString(line, "")
			rpl := ""
			if *replace == "" {
				rpl = "\x1b[33m${0}\x1b[0m"
			} else {
				rpl = fmt.Sprintf("\x1b[33m%s\x1b[0m", *replace)
			}
			cleanLine = re.ReplaceAllString(cleanLine, rpl)
			fmt.Printf("\x1b[36m%4d \x1b[0m%s", numLine, cleanLine)
		}
	}
}

func generateZshCompletion(name string) {
	var cmpl []string
	flag.VisitAll(func(f *flag.Flag) {
		cmpl = append(
			cmpl, fmt.Sprintf("\t'-%s[%s]' \\\n", f.Name, f.Usage))
	})
	fmt.Print(
		fmt.Sprintf("#compdef %s\n\n_arguments -s \\\n%s\n\n",
			name, strings.Join(cmpl, " ")),
	)
}
