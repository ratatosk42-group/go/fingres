# CLI Tools

Find merged with grep -E !

## Installation

browse ```https://git.zug.fr/go/fingres/pipelines?scope=tags``` and download the last artifact.

    unzip artifacts.zip
    ./fingres -self-update

## License

Copyright © 2016  Guillaume Zugmeyer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
