package main

import (
	"encoding/json"
	"fmt"
	"time"
)

// GitLabProject describe a GitLab API project json
type GitLabProject struct {
	ID                int       `json:"id"`
	Description       string    `json:"description"`
	DefaultBranch     string    `json:"default_branch"`
	TagList           []string  `json:"tag_list"`
	SSHUrlToRepo      string    `json:"ssh_url_to_repo"`
	HTTPUrlToRepo     string    `json:"http_url_to_repo"`
	WebURL            string    `json:"web_url"`
	Name              string    `json:"name"`
	NameWithNamespace string    `json:"name_with_namespace"`
	Path              string    `json:"path"`
	PathWithNamespace string    `json:"path_with_namespace"`
	AvatarURL         string    `json:"avatar_url"`
	StarCount         int       `json:"star_count"`
	ForksCount        int       `json:"forks_count"`
	CreatedAt         time.Time `json:"created_at"`
	LastActivity      time.Time `json:"last_activity_at"`
	Link              struct {
		Self         string `json:"self"`
		MergeRequest string `json:"merge_requests"`
		RepoBranches string `json:"repo_branches"`
		Labels       string `json:"labels"`
		Events       string `json:"events"`
		Members      string `json:"members"`
	} `json:"_links"`
	Archived                       bool   `json:"archived"`
	Visibility                     string `json:"visibility"`
	ResolveOutdatedDiffDiscussions bool   `json:"resolve_outdated_diff_discussions"`
	ContainerRegistryEnabled       bool   `json:"container_registry_enabled"`
	IssuesEnabled                  bool   `json:"issues_enabled"`
	MergeRequestsEnabled           bool   `json:"merge_requests_enabled"`
	WikiEnabled                    bool   `json:"wiki_enabled"`
	JobsEnabled                    bool   `json:"jobs_enabled"`
	SnippetsEnabled                bool   `json:"snippets_enabled"`
	SharedRunnersEnabled           bool   `json:"shared_runners_enabled"`
	LFSEnabled                     bool   `json:"lfs_enabled"`
	CreatorID                      int    `json:"creator_id"`
	Namespace                      struct {
		ID                          int    `json:"id"`
		Name                        string `json:"name"`
		Path                        string `json:"path"`
		King                        string `json:"kind"`
		FullPath                    string `json:"full_path"`
		ParentID                    int    `json:"parent_id"`
		MembersCountWithDescendants int    `json:"members_count_with_descendants"`
	} `json:"namespace"`
	ImportStatus                              string   `json:"import_status"`
	PublicJobs                                bool     `json:"public_jobs"`
	CIConfigPath                              string   `json:"ci_config_path"`
	SharedWithGroups                          []string `json:"shared_with_groups"`
	OnlyAllowMergeIfPipelineSucceeds          bool     `json:"only_allow_merge_if_pipeline_succeeds"`
	RequestAccessEnabled                      bool     `json:"request_access_enabled"`
	OnlyAllowMergeIfAllDiscussionsAreResolved bool     `json:"only_allow_merge_if_all_discussions_are_resolved"`
	PrintingMergerRequestLinkEnabled          bool     `json:"printing_merge_request_link_enabled"`
}

// GitLabProjectsList describe a GitLab projects list
type GitLabProjectsList []GitLabProject

// GitLabGroup describe a GitLab API json Group
type GitLabGroup struct {
	ID                   int    `json:"id"`
	Name                 string `json:"name"`
	Path                 string `json:"path"`
	Description          string `json:"description"`
	Visibility           string `json:"visibility"`
	LFSEnabled           bool   `json:"lfs_enabled"`
	AvatarURL            string `json:"avatar_url"`
	WebURL               string `json:"web_url"`
	RequestAccessEnabled bool   `json:"request_access_enabled"`
	FullName             string `json:"full_name"`
	FullPath             string `json:"full_path"`
	ParentID             int    `json:"parent_id"`
}

// GitLabGroupsList describe a GitLab groups list
type GitLabGroupsList []GitLabGroup

// GitLabJob describe a GitLab API json Job
type GitLabJob struct {
	ID         int         `json:"id"`
	Status     string      `json:"status"`
	Stage      string      `json:"stage"`
	Name       string      `json:"name"`
	Ref        string      `json:"ref"`
	Tag        bool        `json:"tag"`
	Coverage   interface{} `json:"coverage"`
	CreatedAt  time.Time   `json:"created_at"`
	StartedAt  time.Time   `json:"started_at"`
	FinishedAt time.Time   `json:"finished_at"`
	Duration   float64     `json:"duration"`
	User       struct {
		ID           int       `json:"id"`
		Name         string    `json:"name"`
		Username     string    `json:"username"`
		State        string    `json:"state"`
		AvatarURL    string    `json:"avatar_url"`
		WebURL       string    `json:"web_url"`
		CreatedAt    time.Time `json:"created_at"`
		Bio          string    `json:"bio"`
		Location     string    `json:"location"`
		Skype        string    `json:"skype"`
		Linkedin     string    `json:"linkedin"`
		Twitter      string    `json:"twitter"`
		WebsiteURL   string    `json:"website_url"`
		Organization string    `json:"organization"`
	} `json:"user"`
	ArtifactsFile struct {
		Filename string `json:"filename"`
		Size     int    `json:"size"`
	} `json:"artifacts_file"`
	Commit struct {
		ID             string    `json:"id"`
		ShortID        string    `json:"short_id"`
		Title          string    `json:"title"`
		CreatedAt      time.Time `json:"created_at"`
		ParentIds      []string  `json:"parent_ids"`
		Message        string    `json:"message"`
		AuthorName     string    `json:"author_name"`
		AuthorEmail    string    `json:"author_email"`
		AuthoredDate   time.Time `json:"authored_date"`
		CommitterName  string    `json:"committer_name"`
		CommitterEmail string    `json:"committer_email"`
		CommittedDate  time.Time `json:"committed_date"`
	} `json:"commit"`
	Runner struct {
		ID          int    `json:"id"`
		Description string `json:"description"`
		Active      bool   `json:"active"`
		IsShared    bool   `json:"is_shared"`
		Name        string `json:"name"`
	} `json:"runner"`
	Pipeline struct {
		ID     int    `json:"id"`
		Sha    string `json:"sha"`
		Ref    string `json:"ref"`
		Status string `json:"status"`
	} `json:"pipeline"`
}

// GitLabJobsList describe a GitLab jobs list
type GitLabJobsList []GitLabJob

// GitLabTag describe a GitLab API json Tag
type GitLabTag struct {
	Name    string `json:"name"`
	Message string `json:"message"`
	Commit  struct {
		ID             string    `json:"id"`
		ShortID        string    `json:"short_id"`
		Title          string    `json:"title"`
		CreatedAt      time.Time `json:"created_at"`
		ParentIds      []string  `json:"parent_ids"`
		Message        string    `json:"message"`
		AuthorName     string    `json:"author_name"`
		AuthorEmail    string    `json:"author_email"`
		AuthoredDate   time.Time `json:"authored_date"`
		CommitterName  string    `json:"committer_name"`
		CommitterEmail string    `json:"committer_email"`
		CommittedDate  time.Time `json:"committed_date"`
	} `json:"commit"`
	Release interface{} `json:"release"`
}

// GitLabTagsList describe a GitLab tags list
type GitLabTagsList []GitLabTag

func getGitLabGroup(name string) (*GitLabGroup, error) {
	rawData, err := httpClientGet(fmt.Sprintf("%s/api/v4/groups", *gitLabURI), false, *gitLabToken)
	if err != nil {
		return nil, err
	}
	var groups []GitLabGroup
	err = json.Unmarshal(rawData, &groups)
	if err != nil {
		return nil, err
	}
	for _, group := range groups {
		if group.Name == name {
			return &group, nil
		}
	}
	return nil, fmt.Errorf("Not found")
}

func getGitLabProjectsList() (*GitLabProjectsList, error) {
	rawData, err := httpClientGet(fmt.Sprintf("%s/api/v4/projects", *gitLabURI), false, *gitLabToken)
	if err != nil {
		return nil, err
	}
	var projects GitLabProjectsList
	err = json.Unmarshal(rawData, &projects)
	if err != nil {
		return nil, err
	}
	return &projects, nil
}

func getGitLabProjectsListByGroup(group *GitLabGroup) (*GitLabProjectsList, error) {
	rawData, err := httpClientGet(fmt.Sprintf("%s/api/v4/groups/%d/projects", *gitLabURI, group.ID), false, *gitLabToken)
	if err != nil {
		return nil, err
	}
	var projects GitLabProjectsList
	err = json.Unmarshal(rawData, &projects)
	if err != nil {
		return nil, err
	}
	return &projects, nil
}

func getGitLabJobsListByProject(project *GitLabProject) (*GitLabJobsList, error) {
	rawData, err := httpClientGet(fmt.Sprintf("%s/api/v4/projects/%d/jobs", *gitLabURI, project.ID), false, *gitLabToken)
	if err != nil {
		return nil, err
	}
	var jobs GitLabJobsList
	json.Unmarshal(rawData, &jobs)
	return &jobs, nil
}

func (jobs *GitLabJobsList) getLastSuccefulJob(tag *GitLabTag) *GitLabJob {
	var (
		lastJobID    int
		lastJobIndex int
	)
	for index, job := range *jobs {
		if job.Status == "success" && job.Tag && job.Ref == tag.Name && job.ID > lastJobID {
			lastJobID = job.ID
			lastJobIndex = index
		}
	}
	if lastJobID != 0 {
		return &(*jobs)[lastJobIndex]
	}
	return nil
}

func getGitLabTagsListByProject(project *GitLabProject) (*GitLabTagsList, error) {
	rawData, err := httpClientGet(fmt.Sprintf("%s/api/v4/projects/%d/repository/tags", *gitLabURI, project.ID), false, *gitLabToken)
	if err != nil {
		return nil, err
	}
	var tags GitLabTagsList
	json.Unmarshal(rawData, &tags)
	return &tags, nil
}
