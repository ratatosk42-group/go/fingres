package main

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	httpClient               *http.Client
	httpClientAcceptRedirect bool
)

func httpClientInit() {

	// Create HTTP Client
	tr := &http.Transport{}
	tr.TLSClientConfig = &tls.Config{}
	jar, _ := cookiejar.New(nil)
	httpClient = &http.Client{Transport: tr, Jar: jar, CheckRedirect: redirectPolicyFunc}

}

func httpClientGet(uri string, httpRedirect bool, token string) ([]byte, error) {
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, err
	}
	if len(token) > 0 {
		req.Header.Add("PRIVATE-TOKEN", token)
	}
	httpClientAcceptRedirect = httpRedirect
	resp, err := httpClient.Do(req)
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf(resp.Status)
	}
	if err != nil {
		return nil, err
	}
	body, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return body, nil
}

func httpClientDownload(uri string, dest string, httpRedirect bool, token string) error {

	var path bytes.Buffer
	path.WriteString(dest)

	out, err := os.Create(path.String())
	if err != nil {
		return err
	}
	defer out.Close()

	headResp, err := http.Head(uri)
	if err != nil {
		return err
	}
	defer headResp.Body.Close()
	size, err := strconv.Atoi(headResp.Header.Get("Content-Length"))
	if err != nil {
		return err
	}

	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return err
	}
	if len(token) > 0 {
		req.Header.Add("PRIVATE-TOKEN", token)
	}
	httpClientAcceptRedirect = httpRedirect

	done := make(chan int64)
	go printDownloadPercent(done, path.String(), int64(size))

	resp, err := httpClient.Do(req)
	if resp.StatusCode != 200 || err != nil {
		return err
	}

	n, err := io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	done <- n

	return nil
}

func redirectPolicyFunc(req *http.Request, via []*http.Request) error {
	if !httpClientAcceptRedirect {
		if len(via) > 0 {
			return fmt.Errorf("Don’t following redirection")
		}
		return nil
	}
	return nil
}

func printDownloadPercent(done chan int64, path string, total int64) {

	var stop bool

	for {
		select {
		case <-done:
			stop = true
		default:

			file, err := os.Open(path)
			if err != nil {
				log.Fatal(err)
			}

			fi, err := file.Stat()
			if err != nil {
				log.Fatal(err)
			}

			size := fi.Size()

			if size == 0 {
				size = 1
			}

			var percent = float64(size) / float64(total) * 100

			fmt.Printf("[%s%s] %3.0f%%\x1b[106D", strings.Repeat("=", int(percent)), strings.Repeat("-", int(100-percent)), percent)
		}

		if stop {
			break
		}

		time.Sleep(100 * time.Millisecond)
	}
}
