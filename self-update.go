package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strconv"
)

func runSelfUpdate() {
	httpClientInit()

	projects, err := getGitLabProjectsList()
	if err != nil {
		log.Fatalln(err)
	}

	for _, project := range *projects {
		if project.Name == BinName {
			sucess := updateProject(&project)
			if sucess {
				os.Exit(0)
			}
			os.Exit(1)
		}
	}
}

func updateProject(project *GitLabProject) bool {

	fmt.Printf("%s: ", project.Name)

	tags, err := getGitLabTagsListByProject(project)
	if err != nil {
		fmt.Printf("\x1b[31mCan’t get last version\x1b[0m %v\n", err)
		return false
	}

	if len(*tags) == 0 {
		fmt.Printf("\x1b[31mCan’t get last version\x1b[0m (no tag)\n")
		return false
	}

	jobs, err := getGitLabJobsListByProject(project)
	if err != nil {
		fmt.Printf("\x1b[31mCan’t get last version\x1b[0m %v\n", err)
		return false
	}

	if len(*jobs) == 0 {
		fmt.Printf("\x1b[31mCan’t get last version\x1b[0m (no job)\n")
		return false
	}

	lastJob := jobs.getLastSuccefulJob(&(*tags)[0])
	if lastJob == nil {
		fmt.Printf("\x1b[31mCan’t get last version\x1b[0m (no valid job)\n")
		return false
	}

	if lastJob.ArtifactsFile.Filename == "" {
		fmt.Printf("\x1b[31mCan’t get last version\x1b[0m (no artifacts)\n")
		return false
	}

	expectedVersion, err := strconv.ParseFloat((*lastJob).Ref[1:], 64)
	if err != nil {
		fmt.Printf("\x1b[31mCan’t get last version\x1b[0m %v\n", err)
		return false
	}

	binPath, err := exec.LookPath(project.Name)
	if err != nil {
		fmt.Printf("\x1b[34mNot found\x1b[0m → v%s: ", strconv.FormatFloat(expectedVersion, 'f', -1, 64))
		binPath = path.Join(os.Getenv("GOPATH"), "/bin", project.Name)
	} else {
		binaryVersion, err := getToolVersion(path.Join(os.Getenv("GOPATH"), "/bin", project.Name), project.Name)
		if err != nil {
			fmt.Printf("\x1b[31m%v\x1b[0m\n", err)
			return false
		}
		expectedPath := path.Join(os.Getenv("GOPATH"), "/bin", project.Name)
		if binPath != expectedPath {
			fmt.Printf("\x1b[31mNot in GOPATH (%s), doing nothing\x1b[0m\n", expectedPath)
			return false
		}
		if expectedVersion <= binaryVersion {
			fmt.Printf("v%s \x1b[32mUp to date\x1b[0m \n", strconv.FormatFloat(binaryVersion, 'f', -1, 64))
			return true
		}
		fmt.Printf("v%s \x1b[33mMust be updated\x1b[0m → v%s: ", strconv.FormatFloat(binaryVersion, 'f', -1, 64), strconv.FormatFloat(expectedVersion, 'f', -1, 64))
	}

	tmpBinPath := path.Join(os.Getenv("GOPATH"), "/bin", project.Name) + ".dist"
	err = httpClientDownload(fmt.Sprintf("%s/builds/artifacts/v%s/raw/%s?job=compile", project.WebURL, strconv.FormatFloat(expectedVersion, 'f', -1, 64), project.Name), tmpBinPath, true, *gitLabToken)
	if err != nil {
		fmt.Printf("\x1b[31m%v\x1b[0m\x1b[K\n", err)
		return false
	}
	fmt.Print("\x1b[K")

	os.Chmod(tmpBinPath, 0755)
	binaryVersion, err := getToolVersion(tmpBinPath, project.Name)
	if err != nil {
		fmt.Printf("\x1b[31m%v\x1b[0m\n", err)
		return false
	}

	if binaryVersion != expectedVersion {
		fmt.Print("\x1b[31mUpdate failed\x1b[0m\n")
		return false
	}

	if err = os.Rename(tmpBinPath, binPath); err != nil {
		fmt.Printf("\x1b[31m%v\x1b[0m\n", err)
		return false
	}
	os.Chmod(binPath, 0755)

	fmt.Print("\x1b[32mSuccess\x1b[0m ")

	fmt.Print("\n")

	return true
}

func getToolVersion(path, name string) (float64, error) {
	tool := exec.Command(path, "-version")
	toolRawVersion, err := tool.Output()
	if err != nil {
		return 0, err
	}

	lines := bytes.Split(toolRawVersion, []byte("\n"))
	re := regexp.MustCompile(fmt.Sprintf(`^%s(?:\.dist)? version ([0-9]+\.[0-9]+)$`, name))

	if len(re.FindSubmatchIndex(lines[0])) > 0 {
		return strconv.ParseFloat(string(re.FindSubmatch(lines[0])[1]), 64)
	}

	return 0, fmt.Errorf("version output not matching version pattern")
}
